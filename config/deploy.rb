
set :stages, %w(production staging)
set :default_stage, "staging"
require 'capistrano/ext/multistage'
set :application, "inventorysolution"
set :repository, "motethansen@bitbucket.org/motethansen/inventorysolution.git"
set :scm, :git

set :use_sudo, false
set :deploy_to, "/home/ec2-user/public"

desc "check production task"
task :check_production do

if stage.to_s == "production"
puts " \n Are you REALLY sure you want to deploy to production?"
puts " \n Enter the password to continue\n "
password = STDIN.gets[0..7] rescue nil
if password != 'takeal00k'
puts "\n !!! WRONG PASSWORD !!!"
exit
end

end

end

before "deploy", "check_production"