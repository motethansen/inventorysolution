require 'test_helper'

class BincontentsControllerTest < ActionController::TestCase
  setup do
    @bincontent = bincontents(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:bincontents)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create bincontent" do
    assert_difference('Bincontent.count') do
      post :create, bincontent: { product_id: @bincontent.product_id, productexpirationdate: @bincontent.productexpirationdate, quantify: @bincontent.quantify, reorderquantity: @bincontent.reorderquantity, storagebin_id: @bincontent.storagebin_id, unitofmeasure: @bincontent.unitofmeasure }
    end

    assert_redirected_to bincontent_path(assigns(:bincontent))
  end

  test "should show bincontent" do
    get :show, id: @bincontent
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @bincontent
    assert_response :success
  end

  test "should update bincontent" do
    put :update, id: @bincontent, bincontent: { product_id: @bincontent.product_id, productexpirationdate: @bincontent.productexpirationdate, quantify: @bincontent.quantify, reorderquantity: @bincontent.reorderquantity, storagebin_id: @bincontent.storagebin_id, unitofmeasure: @bincontent.unitofmeasure }
    assert_redirected_to bincontent_path(assigns(:bincontent))
  end

  test "should destroy bincontent" do
    assert_difference('Bincontent.count', -1) do
      delete :destroy, id: @bincontent
    end

    assert_redirected_to bincontents_path
  end
end
