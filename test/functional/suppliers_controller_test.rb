require 'test_helper'

class SuppliersControllerTest < ActionController::TestCase
  setup do
    @supplier = suppliers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:suppliers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create supplier" do
    assert_difference('Supplier.count') do
      post :create, supplier: { building: @supplier.building, city: @supplier.city, contactperson: @supplier.contactperson, country: @supplier.country, email1: @supplier.email1, email2: @supplier.email2, fax: @supplier.fax, mobile: @supplier.mobile, phone1: @supplier.phone1, phone2: @supplier.phone2, street: @supplier.street, suppliername: @supplier.suppliername, website: @supplier.website, zip: @supplier.zip }
    end

    assert_redirected_to supplier_path(assigns(:supplier))
  end

  test "should show supplier" do
    get :show, id: @supplier
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @supplier
    assert_response :success
  end

  test "should update supplier" do
    put :update, id: @supplier, supplier: { building: @supplier.building, city: @supplier.city, contactperson: @supplier.contactperson, country: @supplier.country, email1: @supplier.email1, email2: @supplier.email2, fax: @supplier.fax, mobile: @supplier.mobile, phone1: @supplier.phone1, phone2: @supplier.phone2, street: @supplier.street, suppliername: @supplier.suppliername, website: @supplier.website, zip: @supplier.zip }
    assert_redirected_to supplier_path(assigns(:supplier))
  end

  test "should destroy supplier" do
    assert_difference('Supplier.count', -1) do
      delete :destroy, id: @supplier
    end

    assert_redirected_to suppliers_path
  end
end
