require 'test_helper'

class WarehouselocationsControllerTest < ActionController::TestCase
  setup do
    @warehouselocation = warehouselocations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:warehouselocations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create warehouselocation" do
    assert_difference('Warehouselocation.count') do
      post :create, warehouselocation: { city: @warehouselocation.city, contactperson: @warehouselocation.contactperson, country: @warehouselocation.country, email: @warehouselocation.email, mobile: @warehouselocation.mobile, number: @warehouselocation.number, phone: @warehouselocation.phone, street: @warehouselocation.street, warehousename: @warehouselocation.warehousename, zip: @warehouselocation.zip }
    end

    assert_redirected_to warehouselocation_path(assigns(:warehouselocation))
  end

  test "should show warehouselocation" do
    get :show, id: @warehouselocation
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @warehouselocation
    assert_response :success
  end

  test "should update warehouselocation" do
    put :update, id: @warehouselocation, warehouselocation: { city: @warehouselocation.city, contactperson: @warehouselocation.contactperson, country: @warehouselocation.country, email: @warehouselocation.email, mobile: @warehouselocation.mobile, number: @warehouselocation.number, phone: @warehouselocation.phone, street: @warehouselocation.street, warehousename: @warehouselocation.warehousename, zip: @warehouselocation.zip }
    assert_redirected_to warehouselocation_path(assigns(:warehouselocation))
  end

  test "should destroy warehouselocation" do
    assert_difference('Warehouselocation.count', -1) do
      delete :destroy, id: @warehouselocation
    end

    assert_redirected_to warehouselocations_path
  end
end
