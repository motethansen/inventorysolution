require 'test_helper'

class ProductsControllerTest < ActionController::TestCase
  setup do
    @product = products(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:products)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create product" do
    assert_difference('Product.count') do
      post :create, product: { deliveryleadtime: @product.deliveryleadtime, firstitemsupplieddate: @product.firstitemsupplieddate, lastitemsupplieddate: @product.lastitemsupplieddate, maximumorderquantity: @product.maximumorderquantity, minimumorderquantity: @product.minimumorderquantity, percentagediscount: @product.percentagediscount, productdescription: @product.productdescription, productname: @product.productname, quantitytype: @product.quantitytype, standardprice: @product.standardprice, supplier_id: @product.supplier_id, totalquantitysuppliedtodate: @product.totalquantitysuppliedtodate, valuesuppliedtodate: @product.valuesuppliedtodate }
    end

    assert_redirected_to product_path(assigns(:product))
  end

  test "should show product" do
    get :show, id: @product
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @product
    assert_response :success
  end

  test "should update product" do
    put :update, id: @product, product: { deliveryleadtime: @product.deliveryleadtime, firstitemsupplieddate: @product.firstitemsupplieddate, lastitemsupplieddate: @product.lastitemsupplieddate, maximumorderquantity: @product.maximumorderquantity, minimumorderquantity: @product.minimumorderquantity, percentagediscount: @product.percentagediscount, productdescription: @product.productdescription, productname: @product.productname, quantitytype: @product.quantitytype, standardprice: @product.standardprice, supplier_id: @product.supplier_id, totalquantitysuppliedtodate: @product.totalquantitysuppliedtodate, valuesuppliedtodate: @product.valuesuppliedtodate }
    assert_redirected_to product_path(assigns(:product))
  end

  test "should destroy product" do
    assert_difference('Product.count', -1) do
      delete :destroy, id: @product
    end

    assert_redirected_to products_path
  end
end
