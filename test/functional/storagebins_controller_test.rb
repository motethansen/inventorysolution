require 'test_helper'

class StoragebinsControllerTest < ActionController::TestCase
  setup do
    @storagebin = storagebins(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:storagebins)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create storagebin" do
    assert_difference('Storagebin.count') do
      post :create, storagebin: { level_z: @storagebin.level_z, row_x: @storagebin.row_x, stack_y: @storagebin.stack_y, storagebinname: @storagebin.storagebinname, warehouselocation_id: @storagebin.warehouselocation_id }
    end

    assert_redirected_to storagebin_path(assigns(:storagebin))
  end

  test "should show storagebin" do
    get :show, id: @storagebin
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @storagebin
    assert_response :success
  end

  test "should update storagebin" do
    put :update, id: @storagebin, storagebin: { level_z: @storagebin.level_z, row_x: @storagebin.row_x, stack_y: @storagebin.stack_y, storagebinname: @storagebin.storagebinname, warehouselocation_id: @storagebin.warehouselocation_id }
    assert_redirected_to storagebin_path(assigns(:storagebin))
  end

  test "should destroy storagebin" do
    assert_difference('Storagebin.count', -1) do
      delete :destroy, id: @storagebin
    end

    assert_redirected_to storagebins_path
  end
end
