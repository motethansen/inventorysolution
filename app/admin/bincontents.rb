ActiveAdmin.register Bincontent do
  index do
    column :products do | p |
      product = Product.find(p.id)
      "#{product.productname}"
    end
    column "Unit of Measure", :unitofmeasure
    column "Quantity", :quantity
    column "Expiry date", :productexpirationdate
    default_actions
  end
  
 filter :products , :as =>  :select, :collection => Product.all.map{|u| [u.productname, u.id]}
 filter :storagebins, :as =>  :select, :collection => Storagebin.all.map{|u| [u.storagebinname, u.id]}
end
