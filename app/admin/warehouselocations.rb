ActiveAdmin.register Warehouselocation do
  index do
		column "Warehouse name", :warehousename
		column :street
		column :contactperson
		column :mobile
		
		default_actions
	end

	filter :warehousename
end
