ActiveAdmin.register Product do
	index do
		column :productname
		column :productdescription
		column :supplier_id do | s |
      		supplier = Supplier.find(s.supplier_id)
      			"#{supplier.suppliername}"
    		end

		default_actions
	end

	form do |f|
  		f.inputs "Products" do
    	f.input :productname
    	f.input :productdescription
    	f.input :supplier_id, :as =>  :select, :collection => Supplier.all.map{|u| [u.suppliername, u.id]}, :include_blank => false 
    	#:select, :collection => Supplier.all.map(&:suppliername), :include_blank => false
  	end
  	f.buttons
end
end
