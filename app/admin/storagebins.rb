ActiveAdmin.register Storagebin do
  index do
  		column "Warehouse", :warehouselocation_id do | w |
      		warehouse = Warehouselocation.find(w.id)
      		"#{warehouse.warehousename}"
      	end
		column "Storage bin name", :storagebinname
		column :row_x
		column :stack_y
		column :level_z

		default_actions
	end
	#filter :warehouselocation_id, :as =>  :select, :collection => Warehouselocation.all.map{|u| [u.warehousename, u.id]}, :include_blank => false 
	filter :storagebinname
	filter :warehouselocations , :as =>  :select, :collection => Warehouselocation.all.map{|w| [w.warehousename, w.id]}

	form do |f|
  		f.inputs "Products" do
    		f.input :storagebinname
    		f.input :row_x
    		f.input :stack_y
    		f.input :level_z
    		
    		f.input :warehouselocation_id, :as =>  :select, :collection => Warehouselocation.all.map{|u| [u.warehousename, u.id]}, :include_blank => false 
    		#:select, :collection => Supplier.all.map(&:suppliername), :include_blank => false
  		end
  		f.buttons
	end
end
