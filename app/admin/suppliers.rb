ActiveAdmin.register Supplier do
  
  index do
		column "Supplier Name",:suppliername
		column "Contact person",:contactperson
		column "Mobile", :mobile
		column "Email", :email1

		default_actions
	end

end
