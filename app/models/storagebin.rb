class Storagebin < ActiveRecord::Base
  attr_accessible :level_z, :row_x, :stack_y, :storagebinname, :warehouselocation_id

  has_many :bincontents
  belongs_to :warehouselocations , :class_name => "Warehouselocation", :foreign_key => "warehouselocation_id"
end
