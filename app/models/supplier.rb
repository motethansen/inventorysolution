class Supplier < ActiveRecord::Base
  attr_accessible :building, :city, :contactperson, :country, :email1, :email2, :fax, :mobile, :phone1, :phone2, :street, :suppliername, :website, :zip

  has_many :products
end
