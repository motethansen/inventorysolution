class Product < ActiveRecord::Base
  attr_accessible :deliveryleadtime, :firstitemsupplieddate, :lastitemsupplieddate, :maximumorderquantity, :minimumorderquantity, :percentagediscount, :productdescription, :productname, :quantitytype, :standardprice, :supplier_id, :totalquantitysuppliedtodate, :valuesuppliedtodate

 belongs_to :suppliers , :class_name => "Supplier", :foreign_key => "supplier_id"
 has_many :bincontents

end
