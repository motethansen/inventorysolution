class Warehouselocation < ActiveRecord::Base
  attr_accessible :city, :contactperson, :country, :email, :mobile, :number, :phone, :street, :warehousename, :zip

  has_many :storagebins
end
