# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130819144630) do

  create_table "active_admin_comments", :force => true do |t|
    t.string   "resource_id",   :null => false
    t.string   "resource_type", :null => false
    t.integer  "author_id"
    t.string   "author_type"
    t.text     "body"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "namespace"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], :name => "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], :name => "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], :name => "index_admin_notes_on_resource_type_and_resource_id"

  create_table "admin_users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "admin_users", ["email"], :name => "index_admin_users_on_email", :unique => true
  add_index "admin_users", ["reset_password_token"], :name => "index_admin_users_on_reset_password_token", :unique => true

  create_table "bincontents", :force => true do |t|
    t.integer  "storagebin_id"
    t.integer  "product_id"
    t.string   "unitofmeasure"
    t.decimal  "quantity"
    t.decimal  "reorderquantity"
    t.date     "productexpirationdate"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "product_imports", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "products", :force => true do |t|
    t.string   "productname"
    t.integer  "supplier_id"
    t.string   "productdescription"
    t.decimal  "standardprice"
    t.integer  "minimumorderquantity"
    t.integer  "maximumorderquantity"
    t.string   "quantitytype"
    t.integer  "deliveryleadtime"
    t.decimal  "valuesuppliedtodate"
    t.integer  "totalquantitysuppliedtodate"
    t.date     "firstitemsupplieddate"
    t.date     "lastitemsupplieddate"
    t.integer  "percentagediscount"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
  end

  create_table "storagebins", :force => true do |t|
    t.integer  "warehouselocation_id"
    t.string   "storagebinname"
    t.integer  "row_x"
    t.integer  "stack_y"
    t.integer  "level_z"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "suppliers", :force => true do |t|
    t.string   "suppliername"
    t.string   "street"
    t.string   "building"
    t.string   "zip"
    t.string   "city"
    t.string   "country"
    t.string   "contactperson"
    t.string   "phone1"
    t.string   "phone2"
    t.string   "fax"
    t.string   "mobile"
    t.string   "email1"
    t.string   "email2"
    t.string   "website"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "warehouselocations", :force => true do |t|
    t.string   "warehousename"
    t.string   "street"
    t.string   "number"
    t.string   "zip"
    t.string   "city"
    t.string   "country"
    t.string   "contactperson"
    t.string   "phone"
    t.string   "mobile"
    t.string   "email"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

end
