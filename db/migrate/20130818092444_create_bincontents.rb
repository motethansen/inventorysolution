class CreateBincontents < ActiveRecord::Migration
  def change
    create_table :bincontents do |t|
      t.integer :storagebin_id
      t.integer :product_id
      t.string :unitofmeasure
      t.decimal :quantify
      t.decimal :reorderquantity
      t.date :productexpirationdate

      t.timestamps
    end
  end
end
