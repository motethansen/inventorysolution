class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :productname
      t.integer :supplier_id
      t.string :productdescription
      t.decimal :standardprice
      t.integer :minimumorderquantity
      t.integer :maximumorderquantity
      t.string :quantitytype
      t.integer :deliveryleadtime
      t.decimal :valuesuppliedtodate
      t.integer :totalquantitysuppliedtodate
      t.date :firstitemsupplieddate
      t.date :lastitemsupplieddate
      t.integer :percentagediscount

      t.timestamps
    end
  end
end
