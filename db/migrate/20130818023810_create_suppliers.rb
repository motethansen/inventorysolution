class CreateSuppliers < ActiveRecord::Migration
  def change
    create_table :suppliers do |t|
      t.string :suppliername
      t.string :street
      t.string :building
      t.string :zip
      t.string :city
      t.string :country
      t.string :contactperson
      t.string :phone1
      t.string :phone2
      t.string :fax
      t.string :mobile
      t.string :email1
      t.string :email2
      t.string :website

      t.timestamps
    end
  end
end
