class CreateWarehouselocations < ActiveRecord::Migration
  def change
    create_table :warehouselocations do |t|
      t.string :warehousename
      t.string :street
      t.string :number
      t.string :zip
      t.string :city
      t.string :country
      t.string :contactperson
      t.string :phone
      t.string :mobile
      t.string :email

      t.timestamps
    end
  end
end
