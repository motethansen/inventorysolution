class RenameQuantifyToQuantity < ActiveRecord::Migration
  change_table :bincontents do |t|
  	t.rename :quantify, :quantity 
  end
end
