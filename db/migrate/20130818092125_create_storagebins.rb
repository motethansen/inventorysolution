class CreateStoragebins < ActiveRecord::Migration
  def change
    create_table :storagebins do |t|
      t.integer :warehouselocation_id
      t.string :storagebinname
      t.integer :row_x
      t.integer :stack_y
      t.integer :level_z

      t.timestamps
    end
  end
end
